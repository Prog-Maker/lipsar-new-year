﻿using UnityEngine;

public class Ragdoll : MonoBehaviour
{
    private Animator _animator;
    private Rigidbody[] bodyes;
    private Collider[] colliders;

    private void Awake()
    {
        bodyes = GetComponentsInChildren<Rigidbody>();
        colliders = GetComponentsInChildren<Collider>();
        _animator = GetComponent<Animator>();
    }

    private void Start()
    {
        ActivateRagdoll();
    }

    public void ActivateRagdoll()
    {
        _animator.enabled = false;
        
        foreach(var body in bodyes)
        {
            body.isKinematic = false;
            body.useGravity = true;
        }

        foreach (var col in colliders)
        {
            col.enabled = true;
        }
    }
}
