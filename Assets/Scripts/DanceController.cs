﻿using UnityEngine;

public class DanceController : MonoBehaviour
{
    private Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
        anim.SetTrigger("Dance" + Random.Range(0, 6));
    }
}
