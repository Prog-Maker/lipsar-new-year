﻿using TMPro;
using UnityEngine;

public class TextLookAtCamera : MonoBehaviour
{
    [SerializeField]
    private TextMeshPro[] textboxes;
    [SerializeField]
    private Transform cam;

    private void Awake()
    {
        textboxes = GetComponentsInChildren<TextMeshPro>();
    }


    // Update is called once per frame
    void LateUpdate()
    {
        foreach (var tb in textboxes)
        {
            tb.transform.LookAt(tb.transform.position + cam.forward);
        }
    }
}
