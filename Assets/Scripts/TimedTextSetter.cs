﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TimedTextSetter : MonoBehaviour
{
    [SerializeField]
    private float timePerChar = 0.5f;

    [SerializeField]
    private UnityEvent actionOnCancelSetText;

    private string text;
    private TMPro.TMP_Text textbox;
    private WaitForSeconds waiter;
    
    void Awake()
    {
        textbox = GetComponent<TMPro.TMP_Text>();
        text = textbox.text;
        textbox.text = "";
        waiter = new WaitForSeconds(timePerChar);
    }

    private void Start()
    {
        StartCoroutine(SetText());
    }

    private IEnumerator SetText()
    {
        for (int i = 0; i < text.Length; i++)
        {
            textbox.text += text[i];
            yield return waiter;
        }

        actionOnCancelSetText?.Invoke();
    }
}
